# Background

This project contains the directions and files needed to add Promethesus
to the defined cluster and started. 

This is mostly done manually

# Directions

Step by step instructions. The general flow is to apply 
a deployment yaml file to the cluster under observation.

## Create rules configMap for Promethesus

kubectl apply -f promethesus-rules-configmap.yaml 

## Deploy the propethesus service

kubectl apply -f promethesus-deploy.yaml

## Check some interesting graphs

Look for the Prometheus endpoint and enter these equations:
'''
sum(http_server_requests_seconds_count{outcome="SUCCESS"}) without (instance)
sum(rate(http_server_requests_seconds_count{outcome="SUCCESS"}[2m])) without (instance)
'''

## Deploy the propethesus service

kubectl apply -f promethesus-deploy.yaml


## Add the Data Source

## Create some Graphs




